
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import apiReducer from '../compineReducer';

const logger = createLogger();
let middleware = [];
middleware = [...middleware, thunk, logger];
export default createStore(
    apiReducer,
    compose(applyMiddleware(...middleware))
);