
const initialState = {
    data: false,
    wishListCount: 0,
    cartLength: 0,
    cartItems: []
};
const cartReducer = (state = initialState, action) => {
    // console.log(action, "action reducer", state);

    switch (action.type) {

        case 'ADD_CART':
            return {
                ...state,
                data: action.payload
            };
        case 'REMOVE_CART':
            return {
                ...state,
                isLoading: action.payload
            };
        default:
            return state;
    }
};

export default cartReducer;