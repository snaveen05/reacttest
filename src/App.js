import React from 'react';
import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useHistory
} from "react-router-dom";
//  pages
import Intial from '../src/components/homePage';
import Aboutpage from '../src/components/aboutPage';

function App() {
  return (
    <div className="App">
      <Router>
        <div>
        {/* <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/about">About</Link>
            </li>
            <li>
              <Link to="/users">Users</Link>
            </li>
          </ul>
        </nav> */}

          <Switch>
            <Route path="/about">
              <Aboutpage />
            </Route>
            <Route path="/">
              <Intial />
            </Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
