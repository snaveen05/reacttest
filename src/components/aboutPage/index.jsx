import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useForm } from "react-hook-form";
import { useLocation } from 'react-router-dom';

function About(Props) {
    const location=useLocation();
    const dispatch = useDispatch();
    const profilrData=location.state;

    useEffect(() => {
        console.log(Props,"location",profilrData);
    }, [])
   
    return (
        <div className="containData">
             <h3 style={{color:'white', fontSize:'25px', marginBottom:'33px'}}>Employee Details Preview</h3>
            <div className='formgroup'><label>Name</label>:<span>{profilrData.name}</span></div>
            <div className='formgroup'> <label>Email</label>: <span>{profilrData.email}</span> </div>
            <div className='formgroup'><label>Phone number</label>:<span>{profilrData.phoneNum}</span></div>
            <div className='formgroup'><label>EmployeeId</label>:<span>{profilrData.employeeId}</span></div>
            <div className='formgroup'>  <label>Date of birth</label>:<span>{profilrData.Dob}</span></div>
            <div className='formgroup'> <label>Date of Join</label>:<span>{profilrData.jDate}</span></div>
            <div className='formgroup'><label>PF number</label>:<span>{profilrData.pfnum}</span></div>
            <div className='formgroup'><label>ProximityId</label>:<span>{profilrData.proximityId}</span></div>
            <div className='formgroup'><label>Languages</label>:<span className='listview'>
                {
                  (profilrData.language.length !=0)?(<>
                  {
                      profilrData.language.map((e,i)=>{
                          return(
                                <div  key={i}>
                                   <span className='listviewdata'> {e.language}</span>
                                </div>
                          )
                      })
                  }
                  </>):null
                }
                </span>
            </div>
            <div className='formgroup'>
                <label>Profile Image</label>:
                <span className='formimagedata'>
                    {
                        (profilrData.photo !="")?(<>
                        <img src={profilrData.photo} />
                        </>):null
                    }
                </span>
            </div> 
        </div>
    );
}

export default About;