import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useForm } from "react-hook-form";
import "./styles.css";
import { Checkbox } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import FileBase64 from 'react-file-base64';

function Home(Props) {
    const dispatch = useDispatch();
    const history = useHistory();
    const programLanguage1=[
      {id:"1",isCheck:false,language:"C"},
      {id:"2",isCheck:false,language:"C++"},
      {id:"3",isCheck:false,language:"C#"},
      {id:"4",isCheck:false,language:"PHP"},
      {id:"5",isCheck:false,language:"SQL"},
      {id:"6",isCheck:false,language:"JAVA"},
    ]
    const [programLanguage ,setProgramLanguage]=useState(programLanguage1);
    const [chooseLanguage ,setChooseLanguage]=useState([]);

    const [name ,setName]=useState("");
    const [exampleRequired ,setExampleRequired]=useState("");
    const [password ,setaPssword]=useState("");
    const [confirmPassword ,setConfirmPassword]=useState("");
    const [email ,setEmail]=useState("");
    const [empId ,setEmpId]=useState("");
    const [proximityId ,setProximityId]=useState("");
    const [phoneNumber ,setPhoneNumber]=useState("");
    const [joinDate ,setJoinDate]=useState("");
    const [dob ,setDob]=useState("");
    const [bankAccount ,setBankAccount]=useState("");
    const [panNo ,setPanNo]=useState("");
    const [pfno ,setPfno]=useState("");
    const [profileImage ,setprofileImage]=useState(null);
     useEffect(() => {
        dispatch({ type: "ADD_CART", payload: true })
    }, [])
    const { register, handleSubmit, errors } = useForm();


  const onSubmit = async (data) =>{
    console.log(data);
      history.push({
        pathname : '/about',
        state :{
          name:data.name,
          email:data.email,
          employeeId:data.employeeId,
          jDate:data.jDate,
          Dob:data.Dob,
          pannum:data.pannum,
          password:data.password,
          pfnum:data.pfnum,
          phoneNum:data.phoneNum,
          proximityId:data.proximityId,
          repassword:data.repassword,
          bankacc:data.bankacc,
          photo:profileImage,
          language:chooseLanguage
        }});
  } 
 
   
    const _languageSelect=(element,i)=>{
      console.log("select data",element);
      let newArray = [...programLanguage];
        newArray[i] = { ...newArray[i], isCheck: !newArray[i].isCheck };
        console.log(newArray);   
        setProgramLanguage(newArray);
        let selectedLanguage=newArray.filter((e,i)=>e.isCheck==true);
        console.log("selected language",selectedLanguage);
        setChooseLanguage(selectedLanguage)
    }

   const getFiles=(files)=>{
         console.log(files[0]);
         if(files.length !=0){
           let strVal = files[0].base64.replace(/"/g, "'");
            console.log("strVal",strVal);
            setprofileImage(strVal);
         }
      }

   
    return (
        <div className="hello">
            <form onSubmit={handleSubmit(onSubmit)}>
            <h3 style={{color:'white', fontSize:'25px', marginBottom:'33px'}}>Employee Contact Form</h3>
                {/* name */}
                <label>Name<span className="star">*</span></label>
                <input name="name" defaultValue={name} ref={register({ required: true })}/>
                {errors.name && <span className="error-call">This name field is required</span>}

                <label>Email<span className="star">*</span></label>
                <input type="email" name="email" defaultValue={email} ref={register({ required: true })} />
                {errors.email && <span className="error-call">This email field is required</span>}

                 {/* name */}
                 <label>Password<span className="star">*</span></label>
                 <input type="password" name="password" defaultValue={password} ref={register({ required: true })} />
                 {errors.password && <span className="error-call">This password field is required</span>}
                  
                  <label>Re-Enter password<span className="star">*</span></label>
                  <input type="password" name="repassword" defaultValue={confirmPassword} ref={register({ required: true })} />
                 {errors.repassword && <span className="error-call">This confirmPassword field is required</span>}

                {/* emp id */}
                <label>Employee Id<span className="star">*</span></label>
                <input name="employeeId" defaultValue={empId} ref={register({ required: true })}/>
                {errors.employeeId && <span className="error-call">This employee id field is required</span>} 
            
                <label>ProximityId</label>
                <input name="proximityId" defaultValue={proximityId} ref={register()}/>
               
                <label>Phone number<span className="star">*</span></label>
                <input name="phoneNum" defaultValue={phoneNumber} ref={register({ required: true })}/>
                {errors.phoneNum && <span className="error-call">This Phone Number field is required</span>}

                <label>Join Date</label>
                <input type="date" name="jDate" defaultValue={joinDate} ref={register()}/>
                {/* {errors.phoneNum && <span className="error-call">This Phone Number field is required</span>} */}

                <label>Date of Birth</label>
                <input type="date" name="Dob" defaultValue={dob} ref={register()}/>
                {/* {errors.Dob && <span className="error-call">This Phone Number field is required</span>} */}

                <label>Bank Account</label>
                <input name="bankacc" defaultValue={bankAccount} ref={register()}/>
                {/* {errors.bankacc && <span className="error-call">This Phone Number field is required</span>} */}
                <label>PAN No</label>
                <input name="pannum" defaultValue={panNo} ref={register()}/>

                <label>P.F No</label>
                <input name="pfnum" defaultValue={pfno} ref={register()}/>

                <label>Technology</label>
                {
                  (programLanguage.length != 0)?(<>
                   {
                  programLanguage.map((element,i)=>{
                      return(
                          <div key={i} style={{color: "#fff", display:'flex'}}>
                          <Checkbox name={element.language} color="primary" onChange={()=>_languageSelect(element,i)} style={{}} />
                          <label >{element.language}</label>
                          </div>
                      )
                  })}
                    </>):null
                }       
                 <FileBase64 multiple={ true } onDone={ getFiles } />
                {
                  (profileImage !=null)?(<>
                  <img style={{height: "90px", width: "100px",borderRadius:"50%",borderColor:"#000"}} src={profileImage} />
                  </>):null
                }
                <input type="submit" />
            </form>
        </div>
    );
}

export default Home;